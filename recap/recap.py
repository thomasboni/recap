#!/usr/bin/env python

"""
This script build an HTML table from json data
"""

import argparse

from sys import exit
from jinja2 import Environment, PackageLoader, select_autoescape
from json import load, JSONDecodeError

TEMPLATES = ['coala.html.j2'] # TODO: get it from templates files list
TEMPLATES_FOLDER = 'templates'

def parse():
    """
    Parse CLI arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        type=argparse.FileType('r'),
                        required=True,
                        help='Input file containing data')
    parser.add_argument('-t', '--template',
                        required=True,
                        choices=TEMPLATES, # TODO: use FileType like json file
                        help='Template to use')
    return parser

def main():
    """
    Main function
    """
    # get args from cli
    args = parse().parse_args()

    # load jinja2 environment
    env = Environment(
        loader=PackageLoader('recap', TEMPLATES_FOLDER),
        autoescape=select_autoescape(['html', 'xml'])
    )

    # load needed template and render it
    # TODO: try ... except
    template = env.get_template('{}'.format(args.template))
    # render template with data
    try:
        result = template.render(data=load(args.file))
    except JSONDecodeError:
        logging.error('No JSON object could be decoded in "{}"'.format(
            args.file))
        exit(1)

    # display the result
    print(result)

if __name__ == '__main__':
    main()

