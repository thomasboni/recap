#!/usr/bin/env python

from glob import glob
from setuptools import setup

TEMPLATE_DIR = 'templates'

templates = glob('{}/*'.format(TEMPLATE_DIR))

setup(name='recap',
      version='0.1.1',
      description='A simple tool which take data as input and produce an output in any format using Jinja2 rendering',
      author='Thomas Boni',
      author_email='thomasboni13@gmail.com',
      url='https://gitlab.com/thomasboni/recap',
      scripts=['cli/recap'],
      packages=['recap'],
      install_requires=['jinja2~=2.10.3'],
      data_files=[('recap/templates', templates)],
     )
