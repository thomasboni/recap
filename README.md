# Recap

![python](https://img.shields.io/badge/python-3.7-blue.svg)
![build status](https://gitlab.com/rocketflow/recap/badges/master/build.svg)
![coverage report](https://gitlab.com/rocketflow/recap/badges/master/coverage.svg)

Simple tool which make prety recap of CI jobs output.

Contents

* [Description](#description)
* [Requirements](#requirements)
* [Getting started](#getting-started)
    * [Installation](#installation)
    * [Configuration](#configuration)
    * [Run it !](#run-it)

## Description

Recap is a light tool based on [Jinja2](https://jinja.palletsprojects.com)
templating tool. It take a json file as input (generally the output of a
CI job) and produce a HTML summary of it.

The idea is to create templates for each CI/CD jobs output and recap just
give the JSON (output from job) as input of template. Recap doesn't make
anything about data. It just gives them to templates.

* [Changelog](CHANGELOG.md)

## Requirements

* Python >= 3.7
* Requirements in [Pipfile](Pipfile)

## Getting started

### Installation

Clone the project
```
git@gitlab.com:rocketflow/recap.git
cd recap
```

Install python environment for development:
```
pipenv install
```

Or install tool to use it on your system:
```
python setup.py install
```

### Configuration

Recap uses template files in `recap/templates` folder. You can use existing
template or create your own following [Jinja2 template
design](https://jinja.palletsprojects.com/en/2.10.x/templates/).

### Run it !

```
usage: recap.py [-h] -f FILE -t {coala.html.j2|...}

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Input file containing data
  -t {coala.html.j2}, --template {coala.html.j2|...}
                        Template to use
```

Template used must be in `recap/templates` folder
